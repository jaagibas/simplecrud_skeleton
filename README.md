# README #

Es un proyecto eclipse. Es el punto de partida para realizar los ejercicios. Desde eclipse se importa como proyecto GIT. Inicialmente dará un error en la configuración de BUILD , es porque no encuentra el servidor TOMCAT 8 sobre el cual se va a ejecutar. Crear un nuevo servidor en la pestaña "Server" del tipo TOMCAT 8. Es requisito tener instalado un TOMCAT 8. 

Para importar el proyecto en un workspace hay dos posibilidades. La primera importar como un proyecto seleccionando la carpeta donde esta el proyecto "SimpleCRUD". La otra es importandolo como proyecto GIT.

Esta aplicación presenta unas páginas JSP(html+css) básicas para minimizar el tiempo de desarrollo de los ejercicios. También se ofrece implementadas las clases JAVA que implementan el modelo del ejercicio. Clases value object (User, Users) y las clases DAO de acceso a la base de datos con una estructura tipo Factory/Wrapper/Facade para facilitar la migración de código a otros motores de base de datos. Actualmente esta implementado parte del DAO de MYSQL.

También se incluyen las librerías jar de mysql.jar y  jstl.jar. También 
se incluye un pequeño script SQL para la creación de la base de datos.

REQUISITOS:
eclipse j2ee bundle (MARS)
plugin GIT